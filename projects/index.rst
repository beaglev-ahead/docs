.. _projects-home:

Projects
********

This is a collection of reasonably well-supported projects useful to Beagle developers.

.. toctree::
   :maxdepth: 1

   /projects/simppru/index.rst
   /projects/bb-config/index.rst
   /boards/beagleconnect/index.rst
